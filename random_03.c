///////////////////////////////////////////////////////////////////////////////
/// SRE
///
/// A harder random number crackme

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char actual_pw[20];


void makePassword( char actual_pw[], char entered_pw[] ) {
   memset( actual_pw, 0, 20 );

   for( char* c = entered_pw ; *c != '\0' ; c++ ) {
      actual_pw[c - entered_pw] = (*c)+1;
   }
   // printf( "actual_pw = [%s]  entered_pw = [%s]\n", actual_pw, entered_pw );
}


int unlock() {
	// 1. Get the user's password
   char entered_pw[20];
   printf( "Enter the password: " );
   fflush(stdout);
   scanf("%16s", (char *)&entered_pw );

	// 2. Make the actual password
   char actual_pw[20];
   makePassword( actual_pw, entered_pw );

	// 3. Do the test
   if( strcmp( entered_pw, actual_pw ) == 0 ) {
      printf( "Access granted: code (%s)\n", actual_pw );
      return 0;
   } else if (entered_pw != actual_pw ) {
      printf( "Access denied\n" );
      return 1;
   }
}

int main() {
   return unlock();
}
